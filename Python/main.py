import subprocess
import time
import helper.config
import logging
import sys
import helper.scrap

def start_wait_Stop(programpath, vmpath):
    vmpath = vmpath.replace("'","")
    #Start Befehl
    logging.info("Start " + vmpath)
    subprocess.call([programpath,"start",vmpath])
    print("START")

    #Warten für Update Download
    for x in range(time_run):
        print(str(x) + "/" + str(time_run),end="\r")
        time.sleep(1)

    #Stop Befehl
    logging.info("Start " + vmpath)
    subprocess.call([programpath,"stop",vmpath])
    print("STOP")

    #Warten für Update Download
    for x in range(60):
        print(str(x) + "/" + str(time_wait),end="\r")
        time.sleep(1)

logger = logging.getLogger()
logger.setLevel(logging.INFO)
handler = logging.FileHandler("logs/VMStartStop.log")
formatter = logging.Formatter('%(asctime)s:%(levelname)s-%(message)s')
handler.setFormatter(formatter)
handler.setLevel(logging.INFO)
logger.addHandler(handler)

conf = helper.config.initconfig()

time_run = int(conf['time_run'])
time_wait = int(conf['time_wait'])
programpath = conf['VM_PROGRAM_PATH']


print("VM Start Stop v0.1")
print("1 = All VMs in the Config Folder")
print("2 = All VMs in the Config List")
eingabe = input("")
if eingabe == "1":
    vmpath = conf['VM_PATH']
    vmpath = vmpath.replace("[","")
    vmpath = vmpath.replace("]","")
    vmpath = vmpath.split(',')
elif eingabe == "2":
    vmpath = conf['VM_FOLDER']
    vmpath = helper.scrap.scrap(vmpath)
else:
    sys.exit(1)


logging.info("Start Programm v0.1")
for vmpath2 in vmpath:
    try:
        start_wait_Stop(programpath,vmpath2)
    except:
        logger.error("Unexpected error:" + str(sys.exc_info()))