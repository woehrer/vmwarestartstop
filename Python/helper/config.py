import configparser
import logging
import sys
import os
from tkinter import messagebox

config = configparser.ConfigParser()
configFile = "config/config.ini"

if not os.path.exists('./config'):
        os.makedirs('./config')
if not os.path.exists('./logs'):
        os.makedirs('./logs')


def initconfig():
    if os.path.isfile(configFile):
        test()
    else:
        create()
    #Konfigdatei initialisieren
    try:
        #Config Datei auslesen
        config.read(configFile)
        conf = config['DEFAULT']
        return conf
    except:
        messagebox.showerror(message="Error while loading the Config file" , title = "Error")
        print("Error while loading the Config file:" + str(sys.exc_info()))
        logging.error("Error while loading the Config file" + str(sys.exc_info()))


def test():
    config.read(configFile)
    conf = config['DEFAULT']
    try:
        conf['time_run']
    except:
        config['DEFAULT']['time_run'] = 3000
    try:
        conf['time_wait']
    except:
        config['DEFAULT']['time_wait'] = 600
    try:
        conf['VM_PROGRAM_PATH']
    except:
        config['DEFAULT']['VM_PROGRAM_PATH'] = 'C:\\Program Files (x86)\\VMware\\VMware Player\\vmrun.exe'
    try:
        conf['VM_PATH']
    except:
        config['DEFAULT']['VM_PATH'] = ['C:\\VMs\\Win10_LTSC_63_TIA_V16\\Windows 10 LTSC.vmx','C:\\VMs\\Win10_LTSC_63_TIA_V16\\Windows 10 LTSC.vmx']
    try:
        conf['vm_folder']
    except:
        config['DEFAULT']['vm_folder'] = 'C:\\VMs\\'

    with open(configFile, 'w') as configfile:
        config.write(configfile)
    

def create():
    
    config['DEFAULT'] = {'time_run' : 3000,
                        'time_wait' : 600,
                        'VM_PROGRAM_PATH' : 'C:\\Program Files (x86)\\VMware\\VMware Player\\vmrun.exe',
                        'VM_PATH' : ['C:\\VMs\\Win10_LTSC_63_TIA_V16\\Windows 10 LTSC.vmx','C:\\VMs\\Win10_LTSC_63_TIA_V16\\Windows 10 LTSC.vmx'],
                        'VM_FOLDER' : 'C:\\VMs\\'
                        }

    with open(configFile, 'w') as configfile:
        config.write(configfile)

